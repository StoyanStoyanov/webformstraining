﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationSecurity.Roles
{
    public interface IRole
    {
        void AddAdministrator();
        void AddRoles();
    }
}
