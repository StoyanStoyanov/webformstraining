﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using WebApplicationSecurity.Models;

namespace WebApplicationSecurity.Roles
{
    public class Role : IRole
    {
        private const string USER_ROLE = "User";
        private const string EDITOR_ROLE = "Editor";
        private const string ADMINISTRATOR_ROLE = "Administrator";
        private const string ADMIN_EMAIL = "admin@security.com";

        private ApplicationDbContext context;

        public Role(ApplicationDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void AddAdministrator()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (userManager.FindByEmail(ADMIN_EMAIL) == null)
            {
                ApplicationUser appUser = new ApplicationUser
                {
                    UserName = ADMIN_EMAIL,
                    Email = ADMIN_EMAIL
                };

                IdentityResult IdUserResult = userManager.Create(appUser, "Aaa!55");
                
                if (!userManager.IsInRole(userManager.FindByEmail(ADMIN_EMAIL).Id, ADMINISTRATOR_ROLE))
                {
                    IdUserResult = userManager.AddToRole(userManager.FindByEmail(ADMIN_EMAIL).Id, ADMINISTRATOR_ROLE);
                }
            }
        }

        public void AddRoles()
        {
            IdentityResult IdRoleResult;

            var roleStore = new RoleStore<IdentityRole>(context);

            var roleMgr = new RoleManager<IdentityRole>(roleStore);

            if (!roleMgr.RoleExists(USER_ROLE))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = USER_ROLE });
            }

            if (!roleMgr.RoleExists(EDITOR_ROLE))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = EDITOR_ROLE });
            }

            if (!roleMgr.RoleExists(ADMINISTRATOR_ROLE))
            {
                IdRoleResult = roleMgr.Create(new IdentityRole { Name = ADMINISTRATOR_ROLE});
            }
        }
    }
}