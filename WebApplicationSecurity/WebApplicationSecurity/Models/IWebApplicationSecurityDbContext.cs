﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace WebApplicationSecurity.Models
{
    public interface IWebApplicationSecurityDbContext
    {
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        IDbSet<IdentityRole> Roles { get; set; }

        IDbSet<ApplicationUser> Users { get; set; }

        int SaveChanges();
    }
}