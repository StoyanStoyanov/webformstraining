﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsersInformation.ascx.cs" Inherits="WebApplicationSecurity.UserControls.UsersInformation" %>

<asp:LoginView ID="LoginView1" runat="server">
    <RoleGroups>
        <asp:RoleGroup Roles="Administrator">
            <ContentTemplate>
                As an Administrator, you may edit and delete user accounts. 
                    Remember: With great power comes great responsibility!
           
            </ContentTemplate>
        </asp:RoleGroup>
        <asp:RoleGroup Roles="User">
            <ContentTemplate>
                You are not a member of the Editor or Administrators roles. 
                Therefore you cannot edit or delete any user information.
            </ContentTemplate>
        </asp:RoleGroup>
        <asp:RoleGroup Roles="Editor">
            <ContentTemplate>
                As an Editor, you may edit users&#39; Email and Name information. 
                Simply click the Edit button, make your changes, and then click Update.
            </ContentTemplate>
        </asp:RoleGroup>
    </RoleGroups>
    <LoggedInTemplate>
        You are not a member of the Admin or Editor roles. Therefore you
        cannot edit or delete any user information.
    </LoggedInTemplate>
    <AnonymousTemplate>
        You are not logged into the system. Therefore you cannot edit or delete any user
        information.
    </AnonymousTemplate>
</asp:LoginView>

<asp:ValidationSummary ID="ValSumTable" runat="server"
    ValidationGroup="UpdateValGroup"
    ShowMessageBox="True"
    ShowSummary="False" />

<asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False"
    OnRowEditing="gvUsers_RowEditing"
    OnRowCancelingEdit="gvUsers_RowCancelingEdit"
    OnRowDeleting="gvUsers_RowDeleting"
    OnRowCreated="gvUsers_RowCreated"
    OnRowUpdating="gvUsers_RowUpdating"
    OnRowDataBound="gvUsers_RowDataBound">
    <Columns>
        <asp:TemplateField ShowHeader="False">
            <EditItemTemplate>
                <asp:LinkButton ID="btnUpdate" runat="server" CausesValidation="True"
                    CommandName="Update" Text="Update"></asp:LinkButton>

                <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="False"
                    CommandName="Cancel" Text="Cancel"></asp:LinkButton>

            </EditItemTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="btnEdit" runat="server" CausesValidation="False"
                    CommandName="Edit" Text="Edit"></asp:LinkButton>

                <asp:LinkButton ID="btnDelete" runat="server" CausesValidation="False"
                    CommandName="Delete" Text="Delete"></asp:LinkButton>

            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
            </EditItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
            </EditItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Email">
            <ItemTemplate>
                <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("Email") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:TextBox runat="server" ID="txtEmail" Text='<%# Bind("Email") %>'></asp:TextBox>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="txtEmail" Display="Dynamic"
                    ErrorMessage="You must provide an email address."
                    SetFocusOnError="True">*</asp:RequiredFieldValidator>

                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="txtEmail" Display="Dynamic"
                    ErrorMessage="The email address you have entered is not valid. Please fix 
               this and try again."
                    SetFocusOnError="True"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*
                </asp:RegularExpressionValidator>
            </EditItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Role">
            <ItemTemplate>
                <asp:Label ID="lblRole" runat="server" Text='<%#: Eval("Role") %>' />
            </ItemTemplate>
            <EditItemTemplate>
                <asp:DropDownList ID="ddlRoles" runat="server">
                    <asp:ListItem Text="User"></asp:ListItem>
                    <asp:ListItem Text="Editor"></asp:ListItem>
                    <asp:ListItem Text="Administrator"></asp:ListItem>
                </asp:DropDownList>
            </EditItemTemplate>
        </asp:TemplateField>
    </Columns>

</asp:GridView>

