﻿using Ninject;
using System;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationSecurity.Repository;

namespace WebApplicationSecurity.UserControls
{
    public partial class UsersInformation : System.Web.UI.UserControl
    {
        [Inject]
        public IUsersRepository UsersRepository { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUserGrid();
            }
        }

        private void BindUserGrid()
        {
            var users = UsersRepository.GetAllUsers();

            foreach (var user in users)
            {
                user.Role = UsersRepository.GetUserRole(user.Id);
            }

            gvUsers.DataSource = users;
            gvUsers.DataBind();
        }

        protected void gvUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvUsers.EditIndex = e.NewEditIndex;
            BindUserGrid();
        }

        protected void gvUsers_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvUsers.EditIndex = -1;
            BindUserGrid();
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Administrator")]
        protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = gvUsers.Rows[e.RowIndex];
            var userId = row.FindControl("lblUserId") as Label;
            
            UsersRepository.DeleteUser(userId.Text);
            
            gvUsers.EditIndex = -1;
            BindUserGrid();
        }

        protected void gvUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvUsers.EditIndex)
            {
                LinkButton EditButton = e.Row.FindControl("btnEdit") as LinkButton;

                LinkButton DeleteButton = e.Row.FindControl("btnDelete") as LinkButton;
                
                EditButton.Visible = (HttpContext.Current.User.IsInRole("Administrator") || HttpContext.Current.User.IsInRole("Editor"));
                DeleteButton.Visible = HttpContext.Current.User.IsInRole("Administrator");
            }
        }
        
        protected void gvUsers_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (!Page.IsValid) { return; }

            GridViewRow row = gvUsers.Rows[e.RowIndex];
            var userId = row.FindControl("lblUserId") as Label;
            var userName = row.FindControl("lblUserName") as Label;

            TextBox email = row.FindControl("txtEmail") as TextBox;
            var selectedRole = row.FindControl("ddlRoles") as DropDownList;

            UsersRepository.EditUser(userId.Text, userName.Text, email.Text.Trim(), selectedRole.SelectedValue);

            gvUsers.EditIndex = -1;
            BindUserGrid();
        }

        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    DropDownList ddl = (DropDownList)e.Row.FindControl("ddlRoles");
                    Label lblUserId = (Label)e.Row.FindControl("lblUserId");
                    ddl.DataSource = UsersRepository.GetAllRoles();
                    ddl.SelectedValue = UsersRepository.GetUserRole(lblUserId.Text);
                    ddl.DataBind();
                }
            }
        }
    }
}