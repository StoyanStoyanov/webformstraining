﻿using Ninject;
using System;
using System.Reflection;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplicationSecurity.Models;
using WebApplicationSecurity.Roles;

namespace WebApplicationSecurity
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            var dbContext = kernel.Get<ApplicationDbContext>();

            // Create the custom role and user.
            Role role = new Role(dbContext);
            role.AddRoles();
            role.AddAdministrator();
        }
    }
}