﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplicationSecurity._Default" %>

<%@ Register Src="~/UserControls/UsersInformation.ascx" TagPrefix="ucUser" TagName="UsersInformation" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <ucUser:UsersInformation runat="server" ID="UsersInformation" />
</asp:Content>
