﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebApplicationSecurity.Models;

namespace WebApplicationSecurity.Repository
{
    public class UsersRepository : IUsersRepository
    {
        private readonly IWebApplicationSecurityDbContext context;
        private readonly UserManager<ApplicationUser> userMgr;

        public UsersRepository(IWebApplicationSecurityDbContext context, UserManager<ApplicationUser> userMgr)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.userMgr = userMgr;
        }

        public string GetUserRole(string id)
        {
            var roles = userMgr.GetRoles(id);
            var role = roles.FirstOrDefault();

            return role;
        }

        public ICollection<ApplicationUser> GetAllUsers()
        {
            var allUsers = context.Users.ToList();

            return allUsers;
        }

        public void DeleteUser(string id)
        {
            var user = userMgr.FindById(id);
            userMgr.Delete(user);
        }

        public List<string> GetAllRoles()
        {
            List<IdentityRole> allRoles = context.Roles.ToList();
            List<string> allRolesNames = new List<string>();

            foreach (var role in allRoles)
            {
                allRolesNames.Add(role.Name);
            }

            return allRolesNames;
        }

        public void EditUser(string id, string userName, string email, string newRoleName)
        {
            var user = context.Users.FirstOrDefault(x => x.Id == id);
            user.UserName = userName;
            user.Email = email;

            var curRoleName = GetUserRole(id);

            if (curRoleName != newRoleName)
            {
                userMgr.RemoveFromRole(id, curRoleName);

                var newRole = context.Roles.FirstOrDefault(x => x.Name == newRoleName);

                userMgr.AddToRole(user.Id, newRole.Name);
            }
            context.Entry(user).State = EntityState.Modified;

            context.SaveChanges();
        }
    }
}