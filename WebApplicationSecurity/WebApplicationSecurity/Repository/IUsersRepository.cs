﻿using System.Collections.Generic;
using WebApplicationSecurity.Models;

namespace WebApplicationSecurity.Repository
{
    public interface IUsersRepository
    {
        string GetUserRole(string id);
        ICollection<ApplicationUser> GetAllUsers();
        void DeleteUser(string id);
        List<string> GetAllRoles();
        void EditUser(string id, string userName, string email, string role);
    }
}
