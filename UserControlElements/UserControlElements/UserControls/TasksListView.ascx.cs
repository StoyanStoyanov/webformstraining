﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using UserControlElements.Models;
using UserControlElements.Repository;
using UserControlElements.Utility;

namespace UserControlElements.UserControls
{
    public partial class TasksListView : System.Web.UI.UserControl
    {
        private Tasks tasks = new Tasks();
        ITaskRepository taskRepository = new TaskRepository();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadList(List<Task> tasks)
        {
            lvTasks.DataSource = tasks;
            lvTasks.DataBind();
        }

        protected void lvTasks_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            lvTasks.EditIndex = e.NewEditIndex;
        }

        protected void lvTasks_ItemCanceling(object sender, ListViewCancelEventArgs e)
        {
            lvTasks.EditIndex = -1;
        }

        protected void lvTasks_ItemUpdating(object sender, ListViewUpdateEventArgs e)
        {
            Label taskId = (Label)lvTasks.EditItem.FindControl("lblTaskIdEdit");

            var editedName = (TextBox)lvTasks.EditItem.FindControl("txtNameEdit");
            var editedCategory = (TextBox)lvTasks.EditItem.FindControl("txtCategoryEdit");
            var editedPriority = (RadioButtonList)lvTasks.EditItem.FindControl("rblPriorityTypeEdit");

            var type = Utilities.TypePriotiryMethod(editedPriority.SelectedValue);

            bool isValidName = editedName.Text.Length >= 2;
            bool isValidCategory = editedCategory.Text.Length >= 2;
            
            if (!isValidName && !isValidCategory)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CategoryAndNameFunction", "ErrorCategoryAndNameMessage()", true);
            }
            else if (!isValidCategory)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CategoryFunction", "ErrorCategoryMessage()", true);
            }
            else if (!isValidName)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "NameFunction", "ErrorNameMessage()", true);
            }
            else
            {
                taskRepository.UpdateTask(taskId.Text, editedName.Text, type, editedCategory.Text);
                lvTasks.EditIndex = -1;
            }

        }

        protected void lvTasks_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            Label id = (Label)lvTasks.Items[e.ItemIndex].FindControl("lblTaskId");

            taskRepository.DeleteTask(id.Text);
        }
    }
}