﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TasksListView.ascx.cs" Inherits="UserControlElements.UserControls.TasksListView" %>
<asp:ListView ID="lvTasks" runat="server"
    ItemType="UserControlElements.Models.Task"
    OnItemCanceling="lvTasks_ItemCanceling"
    OnItemEditing="lvTasks_ItemEditing"
    OnItemUpdating="lvTasks_ItemUpdating"
    OnItemDeleting="lvTasks_ItemDeleting">
    <LayoutTemplate>
        <div class="table-responsive">
            <table class="table table-sm">
                <tr>
                    <td>#</td>
                    <th>Task Name</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Priority Type</th>
                    <th>Category</th>
                </tr>
                <tr runat="server" id="itemPlaceholder" />
            </table>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <tr id="it" style="background-color: white">
            <td>
                <%# Container.DataItemIndex + 1%>
            </td>
            <td>
                <asp:Label ID="lblTaskName" runat="server" Text='<%#: Item.Name %>' />
            </td>
            <td>
                <asp:Label ID="lblCreatedAt" runat="server" Text='<%#: Item.CreatedAt %>' />
            </td>
            <td>
                <asp:Label ID="lblUpdatedAt" runat="server" Text='<%#: Item.UpdatedAt %>' />
            </td>
            <td>
                <asp:Label ID="lblPriorityType" runat="server" Text='<%#: Item.TypeOfPriority %>' />
            </td>
            <td>
                <asp:Label ID="lblCategory" runat="server" Text='<%#: Item.Category %>' />
            </td>
            <td>
                <asp:Label ID="lblTaskId" runat="server" Text='<%#: Item.Id %>' Visible="false" />
            </td>
            <td>
                <asp:Button ID="EditButton" class="btn btn-outline-warning" runat="server" Text="Edit" CommandName="Edit" />
                <asp:Button ID="DeleteButton" class="btn btn-outline-danger" runat="server" Text="Delete" CommandName="Delete" />
            </td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr id="ait" style="background-color: whitesmoke">
            <td>
                <%# Container.DataItemIndex + 1%>
            </td>
            <td>
                <asp:Label ID="lblTaskName" runat="server" Text='<%#: Item.Name %>' />
            </td>
            <td>
                <asp:Label ID="lblCreatedAt" runat="server" Text='<%#: Item.CreatedAt %>' />
            </td>
            <td>
                <asp:Label ID="lblUpdatedAt" runat="server" Text='<%#: Item.UpdatedAt %>' />
            </td>
            <td>
                <asp:Label ID="lblPriorityType" runat="server" Text='<%#: Item.TypeOfPriority %>' />
            </td>
            <td>
                <asp:Label ID="lblCategory" runat="server" Text='<%#: Item.Category %>' />
            </td>
            <td>
                <asp:Label ID="lblTaskId" runat="server" Text='<%#: Item.Id %>' Visible="false" />
            </td>
            <td>
                <asp:Button ID="EditButton" class="btn btn-outline-warning" runat="server" Text="Edit" CommandName="Edit" />
                <asp:Button ID="DeleteButton" class="btn btn-outline-danger" runat="server" Text="Delete" CommandName="Delete" />
            </td>
        </tr>
    </AlternatingItemTemplate>
    <EditItemTemplate>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:TextBox ID="txtNameEdit" runat="server" Text="<%#: Item.Name %>" TextMode="SingleLine" />
            </td>
            <td>
                <asp:Label ID="lblCreateAt" runat="server" Text="<%#: Item.CreatedAt %>" />
            </td>
            <td>
                <asp:Label ID="lblUpdatedAt" runat="server" Text="<%#: Item.UpdatedAt %>" />
            </td>
            <td>
                <asp:RadioButtonList ID="rblPriorityTypeEdit" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem>High-priority</asp:ListItem>
                    <asp:ListItem>Normal-priority</asp:ListItem>
                    <asp:ListItem>Low-priority</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:TextBox ID="txtCategoryEdit" runat="server" Text="<%#: Item.Category %>" />
            </td>
            <td>
                <asp:Label ID="lblTaskIdEdit" runat="server" Text="<%#:Item.Id %> " Visible="false" />
            </td>
            <td>
                <asp:Button ID="btnCancel" class="btn btn-outline-secondary" runat="server" Text="Cancel" CommandName="Cancel" />
                <asp:Button ID="btnUpdate" class="btn btn-outline-success" runat="server" Text="Save" CommandName="Update" />
            </td>
            <td>
                <asp:Label ID="lblErrorMessage" runat="server" />
            </td>
        </tr>
    </EditItemTemplate>
</asp:ListView>

<script type="text/javascript">
    const nameText = 'Name must contains at least 2 symbols!'
    const categoryText = 'Category must contains at least 2 symbols!'

    function ErrorNameMessage() {
        alert(nameText);
    }

    function ErrorCategoryMessage() {
        alert(categoryText);
    }

    function ErrorCategoryAndNameMessage() {
        alert(`${nameText}\r\n${categoryText}`);
    }

    //function ShowEditAndDeleteButtonsOnClick() {
    //    $("#it").on("click", function () {
            
    //        console.log(this.lastChild);
    //    });
    //} 
</script>