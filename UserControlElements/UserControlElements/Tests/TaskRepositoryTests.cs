﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Moq;
using NUnit.Framework;
using UserControlElements.Models;
using UserControlElements.Repository;

namespace UserControlElements.Tests
{
    public class TaskRepositoryTests
    {
        [Test]
        public void CorrectInsertingTask()
        {
            var mockTaskRepository = new Mock<ITaskRepository>();

            var task = new Task
            {
                Id = "StoyanId",
                Name = "Stoyan",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                TypeOfPriority = TaskType.High,
                Category = "Stoyan Task"
            };

            mockTaskRepository.Setup(x => x.InsertTask(task)).Verifiable();
        }

        [Test]
        public void CorrectDeletingTask()
        {
            var mockTaskRepository = new Mock<ITaskRepository>();

            mockTaskRepository.Setup(x => x.DeleteTask("currentId")).Verifiable();
        }

        [Test]
        public void CorrectUpdatingTask()
        {
            var mockTaskRepository = new Mock<ITaskRepository>();

            mockTaskRepository.Setup(x => x.UpdateTask("currentId", "editedName", TaskType.High, "testCategory")).Verifiable();
        }

        [Test]
        public void CorrectGetingTasksTask()
        {
            var mockTaskRepository = new Mock<ITaskRepository>();

            mockTaskRepository.Setup(x => x.GetTasks()).Verifiable();
        }
    }
}