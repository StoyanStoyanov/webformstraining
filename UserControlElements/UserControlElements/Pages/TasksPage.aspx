﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Base.Master" AutoEventWireup="true" CodeBehind="TasksPage.aspx.cs" Inherits="UserControlElements.Pages.TasksPage" %>

<%@ Register Src="~/UserControls/TasksListView.ascx" TagName="TasksListView" TagPrefix="uctask" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBase" runat="server">
    <uctask:TasksListView runat="server" ID="highTask" />
    <br />
    <uctask:TasksListView runat="server" ID="normalTask" />
    <br />
    <uctask:TasksListView runat="server" ID="lowTask" />
    <br />
    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
    <table id="tableId" class="table" style="visibility: hidden">
        <thead class="thead-light">
            <tr>
                <th scope="col">Task Name</th>
                <th scope="col">Priority Type</th>
                <th scope="col">Category</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <asp:TextBox ID="txtInsertName" Text="" runat="server" />
                </td>
                <td>
                    <asp:RadioButtonList ID="rblInsertPriorityType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem>High-priority</asp:ListItem>
                        <asp:ListItem>Normal-priority</asp:ListItem>
                        <asp:ListItem>Low-priority</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:TextBox ID="txtInsertCategory" Text="" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-outline-primary" OnClick="btnInsert_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-outline-secondary" OnClick="btnCancel_Click" />
                </td>
            </tr>
        </tbody>
    </table>
    <script type="text/javascript">
        const nameTextInsert = 'Name must contains at least 2 symbols!';
        const categoryTextInsert = 'Category must contains at least 2 symbols!';
        const priorityTypeTextInsert = 'Please select a priority type!';
        const hidden = "hidden";
        const visible = "visible";

        function ShowTable() {
            document.getElementById("tableId").style.visibility = visible;
        }

        function HideTable() {
            document.getElementById("tableId").style.visibility = hidden;
        }

        function ErrorNameMessage() {
            alert(nameTextInsert);
        }

        function ErrorCategoryMessage() {
            alert(categoryTextInsert);
        }y

        function ErrorCategoryAndNameMessage() {
            alert(`${nameTextInsert}\r\n${categoryTextInsert}`);
        }

        function ErrorBothMessage() {
            alert(`${nameTextInsert}\r\n${categoryTextInsert}\r\n${priorityTypeTextInsert}`);
        }

        function ErrorPriorityMessage() {
            alert(priorityTypeTextInsert);
        }
    </script>
</asp:Content>
