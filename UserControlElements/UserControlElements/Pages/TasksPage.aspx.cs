﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using UserControlElements.Models;
using UserControlElements.Repository;
using UserControlElements.Utility;

namespace UserControlElements.Pages
{
    public partial class TasksPage : System.Web.UI.Page
    {
        private ITaskRepository taskRepository = new TaskRepository();

        private Tasks tasks = new Tasks();
        
        private string dataDirPath = HttpContext.Current.Server.MapPath(@"~/Data");
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists(dataDirPath))
            {
                Directory.CreateDirectory(dataDirPath);
            }

            if (!File.Exists(Utilities.XmlPath))
            {
                Utilities.Serializer(Utilities.XmlPath, tasks);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            tasks = taskRepository.GetTasks();

            var highPriority = tasks.TaskList.Where(x => x.TypeOfPriority.Equals(TaskType.High)).ToList();
            var normalPriority = tasks.TaskList.Where(x => x.TypeOfPriority.Equals(TaskType.Normal)).ToList();
            var lowPriority = tasks.TaskList.Where(x => x.TypeOfPriority.Equals(TaskType.Low)).ToList();

            highTask.LoadList(highPriority);
            normalTask.LoadList(normalPriority);
            lowTask.LoadList(lowPriority);
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var name = txtInsertName.Text;
            var category = txtInsertCategory.Text;

            var priorityType = rblInsertPriorityType.SelectedValue;

            bool isValidName = name.Length >= 2;
            bool isValidCategory = category.Length >= 2;
            bool isSelectedType = priorityType != string.Empty;

            ValidateInputData(name, category, priorityType, isValidName, isValidCategory, isSelectedType);

            txtInsertName.Text = string.Empty;
            txtInsertCategory.Text = string.Empty;
            rblInsertPriorityType.ClearSelection();

            btnAdd.Visible = true;
        }

        private void ValidateInputData(string name, string category, string priorityType, bool isValidName, bool isValidCategory, bool isSelectedType)
        {
            if (!isValidName && !isValidCategory && !isSelectedType)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "BothFunction", "ErrorBothMessage()", true);
            }
            else if (!isValidName && !isValidCategory)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CategoryAndNameFunction", "ErrorCategoryAndNameMessage()", true);
            }
            else if (!isValidCategory)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CategoryFunction", "ErrorCategoryMessage()", true);
            }
            else if (!isValidName)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "NameFunction", "ErrorNameMessage()", true);
            }
            else if (!isSelectedType)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PriorityTypeFunction", "ErrorPriorityMessage()", true);
            }
            else
            {
                var type = Utilities.TypePriotiryMethod(priorityType);

                var task = new Task()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = name,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    TypeOfPriority = type,
                    Category = category
                };
                taskRepository.InsertTask(task);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "HideTable", "HideTable()", true);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowTable", "ShowTable()", true);
            btnAdd.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtInsertName.Text = string.Empty;
            txtInsertCategory.Text = string.Empty;
            rblInsertPriorityType.ClearSelection();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "HideTable", "HideTable()", true);
            btnAdd.Visible = true;
        }
    }
}