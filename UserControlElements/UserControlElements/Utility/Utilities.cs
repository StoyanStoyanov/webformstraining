﻿using System;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using UserControlElements.Models;

namespace UserControlElements.Utility
{
    public class Utilities
    {
        #region Private Member Variables
        private static object _LockObject = new object();
        #endregion

        #region Public Member Variables
        public static readonly string XmlPath = HttpContext.Current.Server.MapPath(@"~/Data/Tasks.xml");
        #endregion

        #region Public Methods
        public static T Deserialize<T>(string path)
            where T : class
        {
            lock (_LockObject)
            {
                T result = null;

                XmlSerializer serializer = new XmlSerializer(typeof(T));

                using (FileStream fileStream = new FileStream(path, FileMode.Open))
                {
                    result = (T)serializer.Deserialize(fileStream);
                }

                return result;
            }
        }

        public static void Serializer<T>(string path, T entity)
            where T : class
        {
            lock (_LockObject)
            {
                var settings = new XmlWriterSettings { Indent = true, IndentChars = "\t" };

                XmlSerializer writer = new XmlSerializer(typeof(T));

                using (FileStream fs = File.Create(path))
                {
                    using (var xmlWriter = XmlWriter.Create(fs, settings))
                    {
                        writer.Serialize(xmlWriter, entity);
                    }
                }
            }
        }

        public static TaskType TypePriotiryMethod(string btnPriority)
        {
            TaskType type = TaskType.High;
            if (btnPriority == "High-priority")
            {
                type = TaskType.High;
            }
            else if (btnPriority == "Normal-priority")
            {
                type = TaskType.Normal;
            }
            else if (btnPriority == "Low-priority")
            {
                type = TaskType.Low;
            }

            return type;
        }
        #endregion
    }
}