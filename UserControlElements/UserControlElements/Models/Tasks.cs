﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace UserControlElements.Models
{
    [Serializable]
    [XmlRoot("TaskDocument")]
    public class Tasks
    {
        #region Public Properties
        [XmlArray]
        public List<Task> TaskList { get; set; } = new List<Task>();
        #endregion
    }
}