﻿using System;
using System.Xml.Serialization;

namespace UserControlElements.Models
{
    public class Task
    {
        #region Public Properties
        [XmlElement("Id")]
        public string Id { get; set; }

        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public DateTime CreatedAt { get; set; }

        [XmlElement]
        public DateTime UpdatedAt { get; set; }

        [XmlElement("Type")]
        public TaskType TypeOfPriority { get; set; }

        [XmlElement]
        public string Category { get; set; }
        #endregion
    }
}