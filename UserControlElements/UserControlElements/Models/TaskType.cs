﻿namespace UserControlElements.Models
{
    public enum TaskType
    {
        High = 1,
        Normal = 2,
        Low = 3
    }
}