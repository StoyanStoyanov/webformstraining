﻿using System;
using System.Collections.Generic;
using System.IO;
using UserControlElements.Models;
using UserControlElements.Utility;

namespace UserControlElements.Repository
{
    public class TaskRepository : ITaskRepository
    {
        private Tasks tasks = new Tasks();
        
        public Tasks GetTasks()
        {
            return GetFromFile();
        }

        public void InsertTask(Task task)
        {
            if (!File.Exists(Utilities.XmlPath))
            {
                var incomeTask = new List<Task>();
                incomeTask.Add(task);

                var tasks = new Tasks
                {
                    TaskList = incomeTask
                };

                SendToFile(tasks);
            }
            else
            {
                tasks = GetTasks();
                tasks.TaskList.Add(task);

                SendToFile(tasks);
            }
        }

        public void UpdateTask(string taskId, string editedName, TaskType type, string editedCategory)
        {
            var tasks = GetTasks();
            var currentTask = tasks.TaskList.Find(x => x.Id == taskId);

            currentTask.Name = editedName;
            currentTask.UpdatedAt = DateTime.Now;
            currentTask.TypeOfPriority = type;
            currentTask.Category = editedCategory;

            SendToFile(tasks);
        }

        public void DeleteTask(string id)
        {
            tasks = GetTasks();

            var itemForDelete = tasks.TaskList.Find(x => x.Id == id);

            tasks.TaskList.Remove(itemForDelete);

            SendToFile(tasks);
        }

        public void SendToFile(Tasks tasks)
        {
            Utilities.Serializer(Utilities.XmlPath, tasks);
        }

        public Tasks GetFromFile()
        {
            Tasks result = null;

            result = Utilities.Deserialize<Tasks>(Utilities.XmlPath);

            return result;
        }
    }
}