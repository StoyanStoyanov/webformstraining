﻿using UserControlElements.Models;

namespace UserControlElements.Repository
{
    public interface ITaskRepository
    {
        Tasks GetTasks();
        void InsertTask(Task task);
        void UpdateTask(string taskId, string editedName, TaskType type, string editedCategory);
        void DeleteTask(string id);
        void SendToFile(Tasks tasks);
        Tasks GetFromFile();
    }
}
