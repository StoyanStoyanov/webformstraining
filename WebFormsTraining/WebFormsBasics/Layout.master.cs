﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsBasics
{
    public partial class Layout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[Login.SESSION_KEY_LOGGED_IN_USER] == null || ((bool)Session[Login.SESSION_KEY_LOGGED_IN_USER] == false))
            {
                Response.Redirect("Login.aspx");
            }

            btnFirstPage.Text = "First page";
            btnSecondPage.Text = "Second page";
            btnLogout.Text = "Logout";

            currentUser.Text = $"Welcome, {Session[Login.SESSION_USER_NAME]}";
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session[Login.SESSION_KEY_LOGGED_IN_USER] = false;
            Response.Redirect("Login.aspx");
        }

        protected void btnFirstPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("FirstPage.aspx");
        }

        protected void btnSecondPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("SecondPage.aspx");
        }
    }
}