﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsBasics
{
    public partial class Login : System.Web.UI.Page
    {
        const string CNTS_SESSION_KEY = "COUNT";

        public const string SESSION_KEY_LOGGED_IN_USER = "IsLoggedIn";
        public const string SESSION_USER_NAME = "Username";

        public string CurrentUser = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string suppliedUserName = txtFirstName.Text;
            string suppliedPassword = txtPassword.Text;
            
            if (!string.IsNullOrWhiteSpace(suppliedUserName) && !string.IsNullOrWhiteSpace(suppliedPassword))
            {
                CurrentUser = suppliedUserName;

                Session[SESSION_USER_NAME] = suppliedUserName;
                Session[suppliedUserName] = suppliedPassword;
                Session[SESSION_KEY_LOGGED_IN_USER] = true;

                Response.Redirect("FirstPage.aspx");
            }
            else
            {
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = "Enter username or password";
            }
        }
    }
}