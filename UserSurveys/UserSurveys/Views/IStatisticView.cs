﻿using System.Collections.Generic;
using UserSurveys.Model;

namespace UserSurveys.Views
{
    public interface IStatisticView
    {
        List<Statistic> Statistics { get; set; }
    }
}
