﻿using System.Collections.Generic;
using UserSurveys.Model;

namespace UserSurveys.Views
{
    public interface IPassedUserInformationView 
    {
        IList<UserServey> UsersServeys { get; set; }
    }
}
