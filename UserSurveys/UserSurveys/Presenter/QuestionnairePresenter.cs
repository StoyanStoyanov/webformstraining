﻿using System.IO;
using System.Linq;
using System.Web;
using UserSurveys.Model;
using UserSurveys.Repository;
using UserSurveys.Utility;
using UserSurveys.Views;

namespace UserSurveys.Presenter
{
    public class QuestionnairePresenter : BasePresenter<IQuestionnaireView>
    {
        private string DirectoryPath = HttpContext.Current.Server.MapPath(@"~/Data");

        private readonly IUserServeysRepository _repository;

        public QuestionnairePresenter(IQuestionnaireView view, INavigationService navigationService, IUserServeysRepository userServeysRepository)
            : base(view, navigationService)
        {
            _repository = userServeysRepository;
        }

        public override void Initialize()
        {
            //Do not have logic to implement here!!!
        }

        public void CreateDirectory()
        {
            if (!Directory.Exists(DirectoryPath))
            {
                Directory.CreateDirectory(DirectoryPath);
            }

            if (!File.Exists(Utilities.XmlPath))
            {
                var usersServey = new UserServeys();
                Utilities.Serializer(Utilities.XmlPath, usersServey);
            }
        }

        public bool IsUniqueUser(string firstName, string lastName)
        {
            var userServeys = _repository.GetUserSurveys();

            return !(userServeys.UsersServeys.Any(p => p.FirstName == firstName) && userServeys.UsersServeys.Any(p => p.LastName == lastName));
        }

        public void InsertDataInUserServey(string userAge, string firstName, string lastName, string sportAnswer, string alcoholAnswer)
        {
            int age = int.Parse(userAge);

            var userServey = new UserServey
            {
                FirstName = firstName,
                LastName = lastName,
                Age = age,
                IsPacticeAnySport = sportAnswer,
                IsDrinkingAlcohol = age >= 18 ? alcoholAnswer : "No"
            };

            _repository.InsertUserServey(userServey);
        }

        public void RedirectToPassedUsersInformation()
        {
            Navigation.GoTo(ViewPages.PassedUsersInformation);
        }
    }
}