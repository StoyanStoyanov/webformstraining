﻿using System.Collections.Generic;
using System.Linq;
using UserSurveys.Model;
using UserSurveys.Repository;
using UserSurveys.Views;

namespace UserSurveys.Presenter
{
    public class StatisticPresenter : BasePresenter<IStatisticView>
    {
        #region Private Constants
        private const string Yes = "Yes";
        private const string No = "No";

        private const string DrinkAlcohol = "Do you drink alcohol?";
        private const string PracticeSport = "Do you practice any sport?";
        #endregion

        private readonly IUserServeysRepository _repository;

        public StatisticPresenter(IStatisticView view, INavigationService navigationService, IUserServeysRepository userServeysRepository)
            : base(view, navigationService)
        {
            _repository = userServeysRepository;
        }

        public override void Initialize()
        {
            var userServeys = new UserServeys();
            userServeys = _repository.GetUserSurveys();

            var statisticData = new List<Statistic>();

            var countOfYesAnswersForAlcohol = userServeys.UsersServeys.Where(x => x.IsDrinkingAlcohol == Yes).Count();
            var countOfNoAnswersForAlcohol = userServeys.UsersServeys.Where(x => x.IsDrinkingAlcohol == No).Count();

            var countOfYesAnswersForSport = userServeys.UsersServeys.Where(x => x.IsPacticeAnySport == Yes).Count();
            var countOfNoAnswersForSport = userServeys.UsersServeys.Where(x => x.IsPacticeAnySport == No).Count();

            Statistic alcoholQuestion = new Statistic(DrinkAlcohol, $"{Yes} - {countOfYesAnswersForAlcohol}", $"{No} - {countOfNoAnswersForAlcohol}");
            Statistic sportQuestionStatistic = new Statistic(PracticeSport, $"{Yes} - {countOfYesAnswersForSport}", $"{No} - {countOfNoAnswersForSport}");

            statisticData.Add(alcoholQuestion);
            statisticData.Add(sportQuestionStatistic);

            View.Statistics = statisticData;
        }
    }
}