﻿using System.Collections.Specialized;
using System.Web;

namespace UserSurveys.Presenter
{
    public class NavigationService : INavigationService
    {
        public void GoTo(ViewPages viewPages)
        {
            GoTo(viewPages, null);
        }

        public void GoTo(ViewPages viewPages, NameValueCollection parameters)
        {
            HttpContext currentContext = HttpContext.Current;
            string redirectUrl = string.Empty;

            switch (viewPages)
            {
                case ViewPages.PassedUsersInformation:
                    redirectUrl = "~/ContentPages/PassedUsersInformationPage.aspx";
                    break;
                default:
                    break;
            }

            currentContext.Response.Redirect(redirectUrl, false);
        }
    }
}