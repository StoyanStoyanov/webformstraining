﻿namespace UserSurveys.Presenter
{
    public interface INavigationService
    {
        void GoTo(ViewPages viewPages);
    }
}
