﻿using System.Collections.Generic;
using System.Linq;
using UserSurveys.Model;
using UserSurveys.Repository;
using UserSurveys.Views;

namespace UserSurveys.Presenter
{
    public class PassedUsersInformationPresenter : BasePresenter<IPassedUserInformationView>
    {
        private readonly IUserServeysRepository _repository;

        public PassedUsersInformationPresenter(IPassedUserInformationView view, INavigationService navigationService, IUserServeysRepository userServeysRepository)
            : base(view, navigationService)
        {
            _repository = userServeysRepository;
        }

        public override void Initialize()
        {
            IList<UserServey> users = new List<UserServey>();
            var userServeys = new UserServeys();

            userServeys = _repository.GetUserSurveys();
            users = userServeys.UsersServeys.ToList();

            View.UsersServeys = users;
        }
    }
}