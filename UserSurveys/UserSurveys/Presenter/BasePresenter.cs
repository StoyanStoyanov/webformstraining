﻿using System;

namespace UserSurveys.Presenter
{
    public abstract class BasePresenter<TView> where TView : class
    {
        private readonly TView _view;

        private readonly INavigationService _navigationService;

        public BasePresenter(TView view)
        {
            _view = view ?? throw new ArgumentNullException("view");
        }

        public BasePresenter(TView view, INavigationService navigationService)
        {
            _view = view ?? throw new ArgumentNullException("view");
            _navigationService = navigationService ?? throw new ArgumentNullException("navigationService");
        }

        public TView View
        {
            get
            {
                return _view;
            }
        }

        public INavigationService Navigation
        {
            get
            {
                return _navigationService;
            }
        }

        public abstract void Initialize();
    }
}