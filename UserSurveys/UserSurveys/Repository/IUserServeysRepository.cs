﻿using UserSurveys.Model;

namespace UserSurveys.Repository
{
    public interface IUserServeysRepository 
    {
        UserServeys GetUserSurveys();
        void InsertUserServey(UserServey userServey);
    }
}
