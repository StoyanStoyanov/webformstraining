﻿using System.Collections.Generic;
using System.IO;
using UserSurveys.Model;
using UserSurveys.Utility;

namespace UserSurveys.Repository
{
    public class UserServeysRepository : IUserServeysRepository
    {
        #region Public Methods
        public UserServeys GetUserSurveys()
        {
            UserServeys result = null;

            result = Utilities.Deserialize<UserServeys>(Utilities.XmlPath);

            return result;
        }

        public void InsertUserServey(UserServey userServey)
        {
            if (!File.Exists(Utilities.XmlPath))
            {
                var us = new List<UserServey>();
                us.Add(userServey);

                var userServeys = new UserServeys
                {
                    UsersServeys = us
                };
                
                Utilities.Serializer(Utilities.XmlPath, userServeys);
            }
            else
            {
                var usersInformation = new UserServeys();
                usersInformation = GetUserSurveys();
                usersInformation.UsersServeys.Add(userServey);

                Utilities.Serializer(Utilities.XmlPath, usersInformation);
            }
        }
        #endregion
    }
}