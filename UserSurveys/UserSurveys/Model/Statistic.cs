﻿namespace UserSurveys.Model
{
    public struct Statistic
    {
        #region Constructors
        public Statistic(string question, string yesAnswer, string noAnswer)
        {
            Question = question;
            YesAnswer = yesAnswer;
            NoAnswer = noAnswer;
        }
        #endregion

        #region Public Properties
        public string Question { get; private set; }
        public string YesAnswer { get; private set; }
        public string NoAnswer { get; private set; }
        #endregion
    }
}