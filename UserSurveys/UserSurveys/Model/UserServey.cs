﻿using System.Xml.Serialization;

namespace UserSurveys.Model
{
    public class UserServey
    {
        #region Public Properties
        [XmlElement("FirstName")]
        public string FirstName { get; set; }

        [XmlElement("LastName")]
        public string LastName { get; set; }

        [XmlElement("Age")]
        public int Age { get; set; }

        [XmlElement("SportAnswer")]
        public string IsPacticeAnySport { get; set; }

        [XmlElement("AlcoholAnswer")]
        public string IsDrinkingAlcohol { get; set; }
        #endregion
    }
}