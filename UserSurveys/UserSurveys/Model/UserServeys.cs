﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace UserSurveys.Model
{
    [Serializable]
    [XmlRoot("UserServeysInformation")]
    public class UserServeys
    {
        #region Public Properties
        [XmlArray("UserServeys")]
        public List<UserServey> UsersServeys { get; set; }
        #endregion
    }
}