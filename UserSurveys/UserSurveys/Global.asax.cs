﻿using Autofac;
using Autofac.Integration.Web;
using System;
using UserSurveys.ContentPages;
using UserSurveys.Presenter;
using UserSurveys.Repository;
using UserSurveys.Views;

namespace UserSurveys
{
    public class Global : System.Web.HttpApplication, IContainerProviderAccessor
    {
        static IContainerProvider _containerProvider;

        public IContainerProvider ContainerProvider
        {
            get { return _containerProvider; }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<NavigationService>().As<INavigationService>();

            builder.RegisterType<UserServeysRepository>().As<IUserServeysRepository>().SingleInstance();

            builder.RegisterType<PassedUsersInformationPresenter>().InstancePerRequest();
            builder.RegisterType<StatisticPresenter>().InstancePerRequest();
            builder.RegisterType<QuestionnairePresenter>().InstancePerRequest();

            builder.RegisterType<QuestionnairePage>().As<IQuestionnaireView>().InstancePerRequest();
            builder.RegisterType<PassedUsersInformationPage>().As<IPassedUserInformationView>().InstancePerRequest();
            builder.RegisterType<StatisticsPage>().As<IStatisticView>().InstancePerRequest();

            _containerProvider = new ContainerProvider(builder.Build());
        }
    }
}