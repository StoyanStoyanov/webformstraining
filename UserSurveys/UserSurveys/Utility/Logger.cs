﻿using System;
using System.IO;

namespace UserSurveys.Utility
{
    public static class Logger
    {
        #region Private Member Variables
        private static object _LockObject = new object();
        #endregion

        #region Public Member Variables
        public static readonly string LogFilePath;
        #endregion

        #region Constructors
        static Logger()
        {
            LogFilePath = @"C:\Data\Log.txt";
        }
        #endregion

        #region Public Methods
        public static void Log (string message)
        {
            lock (_LockObject)
            {
                File.AppendAllText(LogFilePath, $"{DateTime.Now} : {message} \r\n");
            }
        }
        #endregion
    }
}