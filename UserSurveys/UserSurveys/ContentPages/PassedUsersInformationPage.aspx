﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Layout.master" AutoEventWireup="true" CodeBehind="PassedUsersInformationPage.aspx.cs" Inherits="UserSurveys.ContentPages.PassedUsersInformationPage" %>

<asp:Content ID="UsersInformation" ContentPlaceHolderID="cphLayout" runat="server">
    <asp:GridView ID="gridView" runat="server" AllowPaging="True" EnableSortingAndPagingCallbacks="True">
    </asp:GridView>
</asp:Content>