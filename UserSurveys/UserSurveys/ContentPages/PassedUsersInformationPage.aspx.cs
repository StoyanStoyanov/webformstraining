﻿using System;
using System.Collections.Generic;
using UserSurveys.Model;
using UserSurveys.Presenter;
using UserSurveys.Utility;
using UserSurveys.Views;

namespace UserSurveys.ContentPages
{
    public partial class PassedUsersInformationPage : System.Web.UI.Page, IPassedUserInformationView
    {
        public PassedUsersInformationPresenter Presenter { get; set; }

        public IList<UserServey> UsersServeys
        {
            get; set;
        }

        #region Protected Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            Logger.Log("User visited PassedUsersInformationPage.aspx");
            
            Presenter.Initialize();

            gridView.DataSource = Presenter.View.UsersServeys;
            gridView.DataBind();
        }
        #endregion

    }
}