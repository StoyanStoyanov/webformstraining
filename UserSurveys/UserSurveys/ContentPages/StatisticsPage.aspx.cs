﻿using System;
using System.Collections.Generic;
using UserSurveys.Model;
using UserSurveys.Presenter;
using UserSurveys.Utility;
using UserSurveys.Views;

namespace UserSurveys.ContentPages
{
    public partial class StatisticsPage : System.Web.UI.Page, IStatisticView
    {
        public StatisticPresenter Presenter { get; set; }

        List<Statistic> IStatisticView.Statistics { get; set; }

        #region Protected Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            Logger.Log("User visited StatisticsPage.aspx");
            
            Presenter.Initialize();

            gvStatisticTable.DataSource = Presenter.View.Statistics;
            DataBind();
        }
        #endregion
    }
}