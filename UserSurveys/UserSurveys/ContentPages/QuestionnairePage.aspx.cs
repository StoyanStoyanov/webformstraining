﻿using System;
using System.Web.UI;
using UserSurveys.Presenter;
using UserSurveys.Utility;
using UserSurveys.Views;

namespace UserSurveys.ContentPages
{
    public partial class QuestionnairePage : System.Web.UI.Page, IQuestionnaireView
    {
        public QuestionnairePresenter Presenter { get; set; }

        #region Protected Methods
        protected void Page_Init(object sender, EventArgs e)
        {
            Presenter.CreateDirectory();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Logger.Log("User visited QuestionnairePage.aspx");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var isUniqueUser = Presenter.IsUniqueUser(txtFirstName.Text, txtLastName.Text);

            Page.Validate();
            if (Page.IsValid && isUniqueUser)
            {
                Presenter.InsertDataInUserServey(txtAge.Text, txtFirstName.Text, txtLastName.Text, rblSport.SelectedValue, rblAlcohol.SelectedValue);

                Presenter.RedirectToPassedUsersInformation();
            }
            else
            {
                lblUnuniqueUserMessage.Text = "This user already exist.";
            }
        }
        #endregion
    }
}