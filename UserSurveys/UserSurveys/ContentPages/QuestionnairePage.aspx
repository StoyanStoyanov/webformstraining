﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Layout.master" AutoEventWireup="true" CodeBehind="QuestionnairePage.aspx.cs" Inherits="UserSurveys.ContentPages.QuestionnairePage" %>

<asp:Content ID="Questionnaire" ContentPlaceHolderID="cphLayout" runat="server">

    <table class="table table-sm" style="background-color: #99CCFF; margin-right: 0px; font-family: 'Times New Roman'; table-layout: auto; background-image: none; background-repeat: no-repeat;">
        <tr>
            <td style="width: 88px; height: 34px;">
                <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
            </td>
            <td style="width: 505px; height: 34px;" colspan="2">
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="Please enter first name">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 88px">
                <asp:Label ID="lblLastName" runat="server" Text="Last Name"></asp:Label>
            </td>
            <td style="width: 505px" colspan="2">
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Please enter last name">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 88px">
                <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
            </td>
            <td style="width: 505px" colspan="2">
                <asp:TextBox ID="txtAge" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAge" runat="server" ControlToValidate="txtAge" ErrorMessage="Please enter age">*</asp:RequiredFieldValidator>
                <asp:RangeValidator ID="valAgeRange" runat="server" ControlToValidate="txtAge" ErrorMessage="Person must be at least 18 years old" MaximumValue="130" MinimumValue="1" Type="Integer">*</asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 88px; height: 25px;">
                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
            </td>
            <td style="width: 214px; height: 25px;">
                <asp:RadioButtonList ID="rblGender" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem>Male</asp:ListItem>
                    <asp:ListItem>Female</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td style="height: 25px;">
                <asp:RequiredFieldValidator ID="rfvGender" runat="server" ControlToValidate="rblGender" ErrorMessage="Please select a gender">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 27px">
                <asp:Label ID="lblSportQuestion" runat="server" Text="Do you practice any sport?"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 25px">
                <asp:RadioButtonList ID="rblSport" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td style="height: 25px">
                <asp:RequiredFieldValidator ID="rfvSport" runat="server" ErrorMessage="Please select a answer for sport question" ControlToValidate="rblSport">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="trAlcoholQuestion" style="visibility: hidden">
            <td colspan="3" style="height: 24px">
                <asp:Label ID="lblAlcohol" runat="server" Text="Do you drink alcohol?"></asp:Label>
            </td>
        </tr>
        <tr id="trAlcoholAnswer" style="visibility: hidden">
            <td colspan="2" style="height: 25px">
                <asp:RadioButtonList ID="rblAlcohol" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td style="height: 25px">
                <asp:RequiredFieldValidator ID="rfvAlcohol" runat="server" ErrorMessage="Please select a answer for alcohol question" ControlToValidate="rblAlcohol">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 88px; height: 26px;">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                <asp:Label ID="lblUnuniqueUserMessage" runat="server"></asp:Label>
            </td>
            <td style="width: 505px; height: 26px;" colspan="2">
                <asp:ValidationSummary ID="ValidationSummary" runat="server" />
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        var typingTimer;
        var doneTypingInterval = 100;
        var $input = $("#cphBase_cphLayout_txtAge");
        console.log($input);

        $input.on('focusout', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        function doneTyping() {
            let $alcoholQuestion = $("#trAlcoholQuestion");
            let $alcoholRadioButton = $("#trAlcoholAnswer");

            if ($input.val() >= 18) {
                $alcoholQuestion[0].style.visibility = "visible";
                $alcoholRadioButton[0].style.visibility = "visible";
            }
            else {
                $alcoholQuestion[0].style.visibility = "hidden";
                $alcoholRadioButton[0].style.visibility = "hidden";

                $("#cphBase_cphLayout_rblAlcohol :radio:last").prop("checked", true);
            }
        }
    </script>
</asp:Content>
